﻿using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour
{
    [SerializeField] UI_Panel cinematicPanel;
    [SerializeField] UI_Panel dialoguePanel;
    [SerializeField] Text line_npc;
    [SerializeField] Text[] lines_pc;
    [SerializeField] Color selectedColor;

    private Dialogue dialogue;
    private Camera cam;
    private Camera cam_main;
    private Dialogue.DialogueLine currentLine;
    private int currentSelectedLineIndex;
    private bool firstLine;
    private GameInput gi;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        Hide();
    }

    public void ShowDialouge(Dialogue dialogue, Camera cam, Transform playerPivot) {
        if (this.dialogue != null) return;
        this.dialogue = dialogue;
        if (cam_main == null) cam_main = Camera.main;
        this.cam = cam;
        cam.enabled = true;
        cam_main.enabled = false;
        currentLine = dialogue.lines[0];
        currentSelectedLineIndex = 0;
        firstLine = true;
        cinematicPanel.Show();
        PlayerController pc = GameManager.Instance.GetPlayerController();
        pc.transform.position = playerPivot.position;
        pc.transform.rotation = playerPivot.rotation;
        pc.EnterDialouge();
    }

    public void Hide() {
        dialoguePanel.Hide();
    }

    public void ProcessDialouge(float dir) {
        if (currentLine == null) return;
        dialoguePanel.Show();
        line_npc.text = currentLine.npcLine;
        for (int i = 0; i < lines_pc.Length; i++) {
            if(i >= currentLine.pcLines.Length) {
                lines_pc[i].enabled = false;
                continue;
            }
            lines_pc[i].enabled = true;
            lines_pc[i].text = currentLine.pcLines[i].line;
            lines_pc[i].color = (i == currentSelectedLineIndex) ? selectedColor : Color.white;
        }
        if (currentLine.pcLines != null) {
            this.currentSelectedLineIndex = Mathf.Clamp(this.currentSelectedLineIndex - Mathf.CeilToInt(Mathf.Abs(dir)) * (int)Mathf.Sign(dir), 0, currentLine.pcLines.Length - 1);
            //Debug.Log(currentSelectedLineIndex + " - " + currentLine.pcLines.Length);
        }
        if (gi.InteractDown && firstLine == false) {
            if (currentLine.nextID != -1) {
                // next dialouge line (no choices)
                currentLine = dialogue.GetLine(currentLine.nextID);
                currentSelectedLineIndex = 0;
            } else if (currentLine.pcLines != null && currentLine.pcLines.Length != 0) {
                // player selected a reply
                if (currentLine.pcLines[currentSelectedLineIndex].dialoqueActivator) currentLine.pcLines[currentSelectedLineIndex].objectStateController.Activate();
                if (currentLine.pcLines[currentSelectedLineIndex].objectStateController) currentLine.pcLines[currentSelectedLineIndex].objectStateController.Activate();
                currentLine = dialogue.GetLine(currentLine.pcLines[currentSelectedLineIndex].nextID);
                currentSelectedLineIndex = 0;
            } else {
                // end of dialouge
                if (currentLine.dialoqueActivator) currentLine.objectStateController.Activate();
                if (currentLine.objectStateController) currentLine.objectStateController.Activate();
                currentLine = null;
                dialogue = null;
                Hide();
                cam.enabled = false;
                cam_main.enabled = true;
                cinematicPanel.Hide();
                GameManager.Instance.GetPlayerController().ExitDialouge();
            }
        }
        firstLine = false;
    }
}