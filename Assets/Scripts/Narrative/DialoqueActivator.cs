﻿using UnityEngine;

public class DialoqueActivator : MonoBehaviour
{
    [SerializeField] NPCController target;
    [SerializeField] Dialogue dialogue;

    public void Activate() {
        target.SetDialouge(dialogue);
    }
}
