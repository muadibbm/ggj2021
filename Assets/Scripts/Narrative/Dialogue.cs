﻿using UnityEngine;
using System;

public class Dialogue : MonoBehaviour
{
    public DialogueLine[] lines;

    [Serializable]
    public class DialogueLine
    {
        public int ID;
        public string npcLine;
        public int nextID = -1; // -1 refers to no dialogue
        public Choice[] pcLines;
        public DialoqueActivator dialoqueActivator;
        public ObjectStateController objectStateController;

        [Serializable]
        public struct Choice
        {
            public string line;
            public int nextID; // -1 refers to no dialogue
            public DialoqueActivator dialoqueActivator;
            public ObjectStateController objectStateController;
        }
    }

    public DialogueLine GetLine(int ID) {
        for (int i = 0; i < lines.Length; i++)
            if (lines[i].ID == ID) return lines[i];
        return null;
    }
}
