﻿using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] float intensity = 1f;

    private Transform _transform;
    private Coroutine screenShakeOverTimeRoutine;

    public void OnScreenShakeStart(float duration, float delay = 0f) {
        if (_transform == null) _transform = GetComponent<Transform>();
        if (screenShakeOverTimeRoutine != null) StopCoroutine(screenShakeOverTimeRoutine);
        screenShakeOverTimeRoutine = StartCoroutine(ScreenShakeOverTime(duration, delay));
    }

    private Vector3 camStartPosition;

    private IEnumerator ScreenShakeOverTime(float duration, float delay) {
        if (_transform == null) yield break;
        yield return new WaitForSeconds(delay);
        float t = 0;
        camStartPosition = _transform.localPosition;
        while (t < duration) {
            _transform.localPosition = camStartPosition + Random.insideUnitSphere * intensity * Time.deltaTime;
            t += Time.deltaTime;
            yield return null;
        }
        _transform.localPosition = camStartPosition;
        screenShakeOverTimeRoutine = null;
    }

    public void Stop() {
        if (screenShakeOverTimeRoutine != null) {
            StopCoroutine(screenShakeOverTimeRoutine);
            _transform.localPosition = camStartPosition;
        }
        screenShakeOverTimeRoutine = null;
    }
}