﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class IndoorTransparencyController : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] LayerMask layer;

    private Collider col;
    private RaycastHit[] hits = new RaycastHit[1];

    private void Update()
    {
        if(Physics.RaycastNonAlloc(new Ray(transform.position, (target.position - transform.position).normalized), hits, Mathf.Infinity, layer.value) > 0) {
            if(col == null) {
                col = hits[0].collider;
                hits[0].collider.GetComponent<TransparentObject>().SetTransparent(true);
            } else {
                if (col != hits[0].collider) {
                    col.GetComponent<TransparentObject>().SetTransparent(false);
                    col = hits[0].collider;
                    hits[0].collider.GetComponent<TransparentObject>().SetTransparent(true);
                }
            }
        } else {
            if(col != null) {
                col.GetComponent<TransparentObject>().SetTransparent(false);
                col = null;
            }
        }
    }



    private void OnDrawGizmos() {
        Gizmos.DrawLine(transform.position, target.position);
    }
}
