﻿using UnityEngine;

public class WaterGenerator : MonoBehaviour
{
	public bool followPlayer = false;
	[SerializeField] Material material;

	private WaveController wc;
	private int _waveSourceX;
	private int _waveSourceZ;
	private int _speed1;
	private int _wavelength1;
	private int _amplitude1;
	private int _time;
	private int _sparkleSpeed;

	private Transform player;

	private void Awake() {
		wc = GameManager.Instance.GetTool<WaveController>("WaveController");
		_time = Shader.PropertyToID("_TimeValue");
		_waveSourceX = Shader.PropertyToID("_WaveSourceX");
		_waveSourceZ = Shader.PropertyToID("_WaveSourceZ");
		_speed1 = Shader.PropertyToID("_Speed1");
		_wavelength1 = Shader.PropertyToID("_Wavelength1");
		_amplitude1 = Shader.PropertyToID("_Amplitude1");
		_sparkleSpeed = Shader.PropertyToID("_SparkleSpeed");
	}

	private void Update() {
		if (followPlayer) {
			if (ReferenceEquals(player, null)) {
				player = GameManager.Instance.GetPlayer().transform;
			} else {
				Vector3 pos = player.position;
				pos.y = transform.position.y;
				transform.position = pos;
			}
		}
		material.SetFloat(_time, Time.time);
		UpdateMaterial();
	}

	private void UpdateMaterial() {
		material.SetFloat(_speed1, wc.wave1Speed);
		material.SetFloat(_wavelength1, wc.wave1Length);
		material.SetFloat(_amplitude1, wc.wave1Amplitude);
		material.SetFloat(_sparkleSpeed, wc.wave1Amplitude * 0.1f);
		material.SetFloat(_waveSourceX, wc.waveSource.x);
		material.SetFloat(_waveSourceZ, wc.waveSource.z);
	}
}
