﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class TransparentObject : MonoBehaviour
{
    [SerializeField] float transparentTarget = 0.1f;
    [SerializeField] float blendSpeed = 1f;
    [SerializeField] Renderer[] renderers;

    private Coroutine blendRoutine;

    public void SetTransparent(bool active) {
        if (blendRoutine != null) StopCoroutine(blendRoutine);
        blendRoutine = (active) ? StartCoroutine(Blend(StandardShaderUtils.BlendMode.Transparent)) : StartCoroutine(Blend(StandardShaderUtils.BlendMode.Opaque));
    }

    private IEnumerator Blend(StandardShaderUtils.BlendMode mode) {
        float alpha = 0;
        Color color;
        switch(mode) {
            case StandardShaderUtils.BlendMode.Opaque:
                alpha = renderers[0].material.color.a;
                while (alpha < 1f) {
                    for (int i = 0; i < renderers.Length; i++) {
                        color = renderers[i].material.color;
                        color.a = alpha;
                        renderers[i].material.color = color;
                    }
                    alpha = Mathf.Clamp01(alpha + Time.deltaTime * blendSpeed);
                    yield return null;
                }
                for (int i = 0; i < renderers.Length; i++) {
                    renderers[i].material = StandardShaderUtils.ChangeRenderMode(renderers[i].material, StandardShaderUtils.BlendMode.Opaque);
                }
                break;
            case StandardShaderUtils.BlendMode.Transparent:
                for (int i = 0; i < renderers.Length; i++) {
                    renderers[i].material = StandardShaderUtils.ChangeRenderMode(renderers[i].material, StandardShaderUtils.BlendMode.Transparent);
                }
                alpha = renderers[0].material.color.a;
                while (alpha > transparentTarget) {
                    for (int i = 0; i < renderers.Length; i++) {
                        color = renderers[i].material.color;
                        color.a = alpha;
                        renderers[i].material.color = color;
                    }
                    alpha = Mathf.Clamp01(alpha - Time.deltaTime * blendSpeed);
                    yield return null;
                }
                break;
        }
    }
}
