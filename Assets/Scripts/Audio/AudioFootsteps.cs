﻿using UnityEngine;

public class AudioFootsteps : MonoBehaviour
{
    public bool IsPlaying { get; private set; }

    [SerializeField] AudioSource[] foostepsVariations_onStone;
    [SerializeField] AudioSource[] foostepsVariations_onDirt;
    [SerializeField] AudioSource[] foostepsVariations_onGrass;

    [SerializeField] Enum.GroundType defaultGround;
    [SerializeField] float fadeSpeed;
    [SerializeField] float maxVolume = 1f;

    private Enum.GroundType currentGround;
    private AudioSource currentFootsteps;
    private float volume;

    private void Awake() {
        currentGround = defaultGround;
    }

    private void OnTriggerEnter(Collider other) {
        switch(other.tag) {
            case "AUDIO_Stone": currentGround = Enum.GroundType.Stone; break;
            case "AUDIO_Grass": currentGround = Enum.GroundType.Grass; break;
            case "AUDIO_Dirt": currentGround = Enum.GroundType.Dirt; break;
        }
        if (currentFootsteps != null) currentFootsteps.Stop();
        currentFootsteps = null;
    }

    public void Play() {
        if (currentFootsteps == null) {
            switch (currentGround) {
                case Enum.GroundType.Dirt: currentFootsteps = foostepsVariations_onDirt[Random.Range(0, foostepsVariations_onDirt.Length)]; break;
                case Enum.GroundType.Grass: currentFootsteps = foostepsVariations_onGrass[Random.Range(0, foostepsVariations_onGrass.Length)]; break;
                case Enum.GroundType.Stone: currentFootsteps = foostepsVariations_onStone[Random.Range(0, foostepsVariations_onStone.Length)]; break;
            }
        }
        if(currentFootsteps.isPlaying == false) currentFootsteps.Play();
        IsPlaying = true;
    }

    public void Stop() {
        IsPlaying = false;
    }

    private void Update() {
        if (currentFootsteps == null) return;
        if (IsPlaying) {
            volume = Mathf.Clamp(volume + Time.deltaTime * fadeSpeed, 0, maxVolume);
            currentFootsteps.volume = volume;
        } else {
            volume = Mathf.Clamp(volume - Time.deltaTime * fadeSpeed, 0, maxVolume);
            currentFootsteps.volume = volume;
            if (volume == 0) {
                if (currentFootsteps != null) currentFootsteps.Stop();
                currentFootsteps = null;
            }
        }
    }
}
