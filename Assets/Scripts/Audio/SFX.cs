﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SFX : MonoBehaviour
{
    [SerializeField] float lifetime;
    [SerializeField] AudioClip[] sfx;

    private void Awake() {
        StartCoroutine(Play());
    }

    private IEnumerator Play() {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = sfx[Random.Range(0, sfx.Length)];
        audioSource.Play();
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }
}
