﻿using UnityEngine;

public class AudioSourceController : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] float fadeDuration;
    [SerializeField] float targetVolume;
    [SerializeField] Enum.ActivationType type;

    private bool alreadyActivated;

    private void OnEnable() {
        if (type == Enum.ActivationType.OnEnable) Activate();
    }

    private void OnTriggerEnter(Collider other) {
        if (type == Enum.ActivationType.OnTriggerEnter) Activate();
    }

    private void OnTriggerExit(Collider other) {
        if (type == Enum.ActivationType.OnTriggerExit) Activate();
    }

    public void Activate() {
        if (alreadyActivated) return;
        GameManager.Instance.GetTool<AudioManager>("AudioManager").Fade(audioSource, targetVolume, fadeDuration);
        alreadyActivated = true;
    }
}
