﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ColliderVisualizer : MonoBehaviour
{
    // TODO : provide support for other types of colliders
    private BoxCollider col;

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, Vector3.one * 0.1f);
        Gizmos.color = Color.green;
        if (col == null) col = GetComponent<BoxCollider>();
        Gizmos.DrawLine(transform.position, transform.position + transform.rotation * col.center);
        Gizmos.DrawCube(transform.position + transform.rotation * col.center, 
            new Vector3(transform.lossyScale.x * col.size.x, transform.lossyScale.y * col.size.y, transform.lossyScale.y * col.size.y));
    }
}
