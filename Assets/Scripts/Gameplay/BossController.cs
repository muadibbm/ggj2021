﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour
{
    public bool IsInvincible { get; private set; }

    [SerializeField] float speedWalk;
    [SerializeField] float speedRun;
    [SerializeField] float speedAngular;
    [SerializeField] float stoppingDistance;
    [SerializeField] Transform arenaCenter;
    [SerializeField] float meleeRange;

    [SerializeField] float durationMeleeAttack;
    [SerializeField] float durationRangedAttack;
    [SerializeField] float duractionTaunt1;
    [SerializeField] float duractionTaunt2;
    [SerializeField] float durationJumpAttack;
    [SerializeField] float durationAOEAttack;
    [SerializeField] float durationForPlayerToRecover = 4f;
    [SerializeField] float meleeAnimHitMoment = 2.5f;

    [SerializeField] Renderer Rock;
    [SerializeField] GameObject [] rockProjectiles;
    [SerializeField] GameObject[] fallingRockProjectiles;
    [SerializeField] Transform[] rockTargets;
    [SerializeField] float rockFallingHeight;

    [SerializeField] float healthMax;
    [SerializeField] float healthToStage2;
    [SerializeField] float healthToStage3;
    [SerializeField] float delayBetweenStages = 1f;
    [SerializeField] UnityEngine.UI.Image UI_health;
    [SerializeField] UI_Panel gameOver_UI;

    [SerializeField] float damageMeleeAttack;
    [SerializeField] float damageJumpAttack;
    [SerializeField] float damageJumpAOE;

    [SerializeField] HitBox hitBoxRightHand;
    [SerializeField] HitBox hitBoxLeftHand;
    [SerializeField] HitBox hitBoxBody;

    [SerializeField] GameObject vfx_impact;
    [SerializeField] Transform vfx_spawn_landing;

    [SerializeField] CameraShake cameraShake;

    [SerializeField] AudioSource music_intro;
    [SerializeField] AudioSource music_stage1;
    [SerializeField] AudioSource music_stage2;
    [SerializeField] AudioSource music_stage3;
    [SerializeField] AudioSource music_transition;

    [SerializeField] AudioSource sfx_die;
    [SerializeField] AudioSource [] sfx_takeDamage;
    [SerializeField] AudioSource [] sfx_walk;
    [SerializeField] AudioSource sfx_run;
    [SerializeField] AudioSource sfx_taunt1;
    [SerializeField] AudioSource sfx_taunt2;
    [SerializeField] AudioSource sfx_attackRanged;
    [SerializeField] AudioSource sfx_attackJump;
    [SerializeField] AudioSource sfx_AOE;
    [SerializeField] AudioSource [] sfx_jump;
    [SerializeField] AudioSource [] sfx_attackMeleeVariations;
    [SerializeField] AudioSource[] vo_lines;
    [SerializeField] AudioSource vo_intro;
    [SerializeField] ObjectStateController osc_intro;
    [SerializeField] Transform firstTimeCombatSpawn;

    public Transform Hips;

    private Animator animator;
    private AudioManager am;
    private PlayerController player;

    private Transform currentDestination;
    private Projectile currentProjectile;
    private float currentSpeed;
    private float health;

    private readonly string animWalk = "isWalking";
    private readonly string animRun = "isCharging";
    private readonly string animMeleeAttack = "AttackMelee";
    private readonly string animRangedAttack = "AttackRanged";
    private readonly string animAOEAttack = "AttackAOE";
    private readonly string animJumpAttack = "AttackJump";
    private readonly string animTaunt1 = "Taunt1";
    private readonly string animTaunt2 = "Taunt2";
    private readonly string animDead = "isDead";
    private int animParamWalk;
    private int animParamRun;
    private int animParamMeleeAttack;
    private int animParamRangedAttack;
    private int animParamAOEAttack;
    private int animParamJumpAttack;
    private int animParamTaunt1;
    private int animParamTaunt2;
    private int animParamDead;

    private Coroutine waitForPlayerRoutine;

    private void Awake() {
        player = GameManager.Instance.GetPlayerController();
        am = GameManager.Instance.GetTool<AudioManager>("AudioManager");
        animator = GetComponentInChildren<Animator>();
        animParamWalk = Animator.StringToHash(animWalk);
        animParamRun = Animator.StringToHash(animRun);
        animParamMeleeAttack = Animator.StringToHash(animMeleeAttack);
        animParamRangedAttack = Animator.StringToHash(animRangedAttack);
        animParamAOEAttack = Animator.StringToHash(animAOEAttack);
        animParamJumpAttack = Animator.StringToHash(animJumpAttack);
        animParamTaunt1 = Animator.StringToHash(animTaunt1);
        animParamTaunt2 = Animator.StringToHash(animTaunt2);
        animParamDead = Animator.StringToHash(animDead);
        Rock.enabled = false;
        health = healthMax;
        UpdateHealth();
    }

    private void Start() {
        StartCoroutine(FirstTimeStartCombat());
    }

    private IEnumerator FirstTimeStartCombat() {
        currentDestination = firstTimeCombatSpawn;
        currentSpeed = speedWalk;
        animator.SetBool(animParamWalk, true);
        AudioSource _sfx_walk = sfx_walk[Random.Range(0, sfx_walk.Length)];
        _sfx_walk.Play();
        yield return new WaitUntil(() => currentDestination == null);
        _sfx_walk.Stop();
        animator.SetBool(animParamWalk, false);
        vo_intro.Play();
        yield return new WaitForSeconds(6f);
        StartCombat();
        player.StartCombat();
        osc_intro.Activate();
    }

    public void Respawn() {
        transform.position = firstTimeCombatSpawn.position;
        transform.rotation = firstTimeCombatSpawn.rotation;
        health = healthMax;
        UpdateHealth();
    }

    public void StartCombat() {
        StartCoroutine(BossBattleStage1());
    }

    private IEnumerator BossBattleStage1() {
        if (music_stage1.isPlaying == false) {
            am.Fade(music_stage1, 1f, 5f);
            am.Fade(music_intro, 0f, 5f);
        }
        currentSpeed = speedWalk;
        float time = 0;
        if (Vector3.Distance(player.transform.position, transform.position) > meleeRange) {
            animator.SetTrigger(animParamTaunt1);
            if(sfx_taunt1.isPlaying == false) sfx_taunt1.Play();
            yield return new WaitForSeconds(duractionTaunt1);
            animator.SetTrigger(animParamRangedAttack);
            sfx_attackRanged.Play();
            while (time < durationRangedAttack) {
                UpdateRotation(player.transform.position, time / durationRangedAttack);
                Rock.enabled = (time > 1f && time < 2.65f);
                if(time > 2.65f) DoRangedAttack();
                time += Time.deltaTime;
                yield return null;
            }
        }
        currentProjectile = null;
        currentDestination = player.transform;
        animator.SetBool(animParamWalk, true);
        AudioSource _sfx_walk = sfx_walk[Random.Range(0, sfx_walk.Length)];
        _sfx_walk.Play();
        yield return new WaitUntil(() => currentDestination == null);
        _sfx_walk.Stop();
        animator.SetBool(animParamWalk, false);
        animator.SetTrigger(animParamMeleeAttack);
        sfx_attackMeleeVariations[Random.Range(0, sfx_attackMeleeVariations.Length)].Play();
        time = 0;
        bool hitBoxActive = false;
        bool vfxshown = false;
        while (time < durationMeleeAttack) {
            UpdateRotation(player.transform.position, time / durationMeleeAttack);
            if ((time / durationMeleeAttack) > 0.5f) {
                if (hitBoxActive == false) {
                    hitBoxLeftHand.Enable(damageMeleeAttack);
                    hitBoxRightHand.Enable(damageMeleeAttack);
                    hitBoxBody.Enable(damageMeleeAttack);
                    hitBoxActive = true;
                }
            }
            if (time > meleeAnimHitMoment) {
                if (vfxshown == false) {
                    Instantiate(vfx_impact, vfx_spawn_landing.position + vfx_spawn_landing.forward * 4f, Quaternion.identity);
                    cameraShake.OnScreenShakeStart(1.5f);
                    vfxshown = true;
                }
            }
            time += Time.deltaTime;
            yield return null;
        }
        hitBoxLeftHand.Disable();
        hitBoxRightHand.Disable();
        hitBoxBody.Disable();
        yield return new WaitForSeconds(delayBetweenStages);
        if (health < healthToStage2) {
            StartCoroutine(BossBattleStage2());
        } else {
            StartCoroutine(BossBattleStage1());
        }
    }

    private IEnumerator BossBattleStage2() {
        if (music_stage2.isPlaying == false) {
            am.Transition(music_stage1, music_stage2, music_transition);
        }
        currentSpeed = speedRun;
        float time = 0;
        bool hitBoxActive;
        bool vfxshown;
        if (Vector3.Distance(player.transform.position, transform.position) > meleeRange) {
            int random = Random.Range(0, 2);
            if (random == 0) {
                animator.SetTrigger(animParamTaunt1);
                if (sfx_taunt1.isPlaying == false) sfx_taunt1.Play();
                yield return new WaitForSeconds(duractionTaunt1);
                animator.SetTrigger(animParamRangedAttack);
                sfx_attackRanged.Play();
                while (time < durationRangedAttack) {
                    UpdateRotation(player.transform.position, time / durationRangedAttack);
                    Rock.enabled = (time > 1f && time < 2.65f);
                    if (time > 2.65f) DoRangedAttack();
                    time += Time.deltaTime;
                    yield return null;
                }
            } else {
                animator.SetTrigger(animParamTaunt2);
                if (sfx_taunt2.isPlaying == false) sfx_taunt2.Play();
                yield return new WaitForSeconds(duractionTaunt2);
                animator.SetBool(animParamJumpAttack, true);
                Vector3 target = player.transform.position;
                target.y = transform.position.y;
                float dist = (target - transform.position).magnitude;
                Vector3 startPost = transform.position;
                hitBoxActive = false;
                vfxshown = false;
                sfx_attackJump.Play();
                while (time < durationJumpAttack) {
                    UpdateRotation(target, time / durationJumpAttack);
                    if (time > 0.4f && time < 1.5f) transform.position = Vector3.Lerp(startPost, target, GetRelativeFloatValue(0, 1f, 0.4f, 1.5f, time) / 1f);
                    if (time > 0.5f) {
                        if (hitBoxActive == false) {
                            hitBoxBody.Enable(damageJumpAttack);
                            hitBoxActive = true;
                        }
                    }
                    if (time > 1.5f) {
                        if (vfxshown == false) {
                            Instantiate(vfx_impact, vfx_spawn_landing.position, Quaternion.identity);
                            cameraShake.OnScreenShakeStart(1.5f);
                            vfxshown = true;
                        }
                    }
                    time += Time.deltaTime;
                    yield return null;
                }
                animator.SetBool(animParamJumpAttack, false);
                hitBoxBody.Disable();
            }
        }
        currentDestination = player.transform;
        animator.SetBool(animParamRun, true);
        sfx_run.Play();
        yield return new WaitUntil(() => currentDestination == null);
        sfx_run.Stop();
        animator.SetBool(animParamRun, false);
        animator.SetTrigger(animParamMeleeAttack);
        sfx_attackMeleeVariations[Random.Range(0, sfx_attackMeleeVariations.Length)].Play();
        time = 0;
        vfxshown = hitBoxActive = false;
        while (time < durationMeleeAttack) {
            UpdateRotation(player.transform.position, time / durationMeleeAttack);
            if ((time / durationMeleeAttack) > 0.5f) {
                if (hitBoxActive == false) {
                    hitBoxLeftHand.Enable(damageMeleeAttack);
                    hitBoxRightHand.Enable(damageMeleeAttack);
                    hitBoxBody.Enable(damageMeleeAttack);
                    hitBoxActive = true;
                }
            }
            if (time > meleeAnimHitMoment) {
                if (vfxshown == false) {
                    Instantiate(vfx_impact, vfx_spawn_landing.position + vfx_spawn_landing.forward * 4f, Quaternion.identity);
                    cameraShake.OnScreenShakeStart(1.5f);
                    vfxshown = true;
                }
            }
            time += Time.deltaTime;
            yield return null;
        }
        hitBoxLeftHand.Disable();
        hitBoxRightHand.Disable();
        hitBoxBody.Disable();
        yield return new WaitForSeconds(delayBetweenStages);
        if (health < healthToStage3) {
            StartCoroutine(BossBattleStage3());
        } else {
            StartCoroutine(BossBattleStage2());
        }
    }

    private IEnumerator BossBattleStage3() {
        if (music_stage3.isPlaying == false) {
            am.Transition(music_stage2, music_stage3, music_transition);
        }
        currentSpeed = speedRun;
        float time = 0;
        bool hitBoxActive = false;
        bool vfxshown = false;
        int random;
        random = Random.Range(0, 2);
        if (random == 0) {
            currentDestination = arenaCenter;
            sfx_run.Play();
            animator.SetBool(animParamRun, true);
            yield return new WaitUntil(() => currentDestination == null);
            animator.SetBool(animParamRun, false);
            sfx_run.Stop();
            animator.SetBool(animParamAOEAttack, true);
            StartCoroutine(DoVFXAndHitBoxActive());
            StartCoroutine(DoAOEAttack());
            yield return new WaitForSeconds(durationAOEAttack);
            animator.SetBool(animParamAOEAttack, false);
        }
        if (Vector3.Distance(player.transform.position, transform.position) > meleeRange) {
            random = Random.Range(0, 2);
            if (random == 0) {
                animator.SetTrigger(animParamTaunt1);
                if (sfx_taunt1.isPlaying == false) sfx_taunt1.Play();
                yield return new WaitForSeconds(duractionTaunt1);
                animator.SetTrigger(animParamRangedAttack);
                sfx_attackRanged.Play();
                while (time < durationRangedAttack) {
                    UpdateRotation(player.transform.position, time / durationRangedAttack);
                    Rock.enabled = (time > 1f && time < 2.65f);
                    if (time > 2.65f) DoRangedAttack();
                    time += Time.deltaTime;
                    yield return null;
                }
            } else {
                animator.SetTrigger(animParamTaunt2);
                if (sfx_taunt2.isPlaying == false) sfx_taunt2.Play();
                yield return new WaitForSeconds(duractionTaunt2);
                animator.SetBool(animParamJumpAttack, true);
                Vector3 target = player.transform.position;
                target.y = transform.position.y;
                float dist = (target - transform.position).magnitude;
                Vector3 startPost = transform.position;
                hitBoxActive = false;
                vfxshown = false;
                sfx_attackJump.Play();
                while (time < durationJumpAttack) {
                    UpdateRotation(target, time / durationJumpAttack);
                    if (time > 0.4f && time < 1.5f) transform.position = Vector3.Lerp(startPost, target, GetRelativeFloatValue(0, 1f, 0.4f, 1.5f, time) / 1f);
                    if (time > 0.5f) {
                        if (hitBoxActive == false) {
                            hitBoxBody.Enable(damageJumpAttack);
                            hitBoxActive = true;
                        }
                    }
                    if(time > 1.5f) {
                        if(vfxshown == false) {
                            Instantiate(vfx_impact, vfx_spawn_landing.position, Quaternion.identity);
                            cameraShake.OnScreenShakeStart(1.5f);
                            vfxshown = true;
                        }
                    }
                    time += Time.deltaTime;
                    yield return null;
                }
                hitBoxBody.Disable();
                animator.SetBool(animParamJumpAttack, false);
            }
        }
        currentDestination = player.transform;
        animator.SetBool(animParamRun, true);
        sfx_run.Play();
        yield return new WaitUntil(() => currentDestination == null);
        animator.SetBool(animParamRun, false);
        sfx_run.Stop();
        animator.SetTrigger(animParamMeleeAttack);
        sfx_attackMeleeVariations[Random.Range(0, sfx_attackMeleeVariations.Length)].Play();
        time = 0;
        vfxshown = hitBoxActive = false;
        while (time < durationMeleeAttack) {
            UpdateRotation(player.transform.position, time / durationMeleeAttack);
            if ((time / durationMeleeAttack) > 0.5f) {
                if (hitBoxActive == false) {
                    hitBoxLeftHand.Enable(damageMeleeAttack);
                    hitBoxRightHand.Enable(damageMeleeAttack);
                    hitBoxBody.Enable(damageMeleeAttack);
                    hitBoxActive = true;
                }
            }
            if (time > meleeAnimHitMoment) {
                if (vfxshown == false) {
                    Instantiate(vfx_impact, vfx_spawn_landing.transform.position + vfx_spawn_landing.forward * 4f, Quaternion.identity);
                    cameraShake.OnScreenShakeStart(1.5f);
                    vfxshown = true;
                }
            }
            time += Time.deltaTime;
            yield return null;
        }
        hitBoxLeftHand.Disable();
        hitBoxRightHand.Disable();
        hitBoxBody.Disable();
        yield return new WaitForSeconds(delayBetweenStages);
        StartCoroutine(BossBattleStage3());
        // TODO : death
    }

    private void DoRangedAttack() {
        if (currentProjectile != null) return;
        currentProjectile = Instantiate(rockProjectiles[Random.Range(0, rockProjectiles.Length)], Rock.transform.position, Rock.transform.rotation).GetComponent<Projectile>();
        currentProjectile.Shoot((player.transform.position - transform.position).normalized, true, player.transform.position);
    }

    private IEnumerator DoVFXAndHitBoxActive() {
        float totalJump = (int) (durationAOEAttack / 3.167f);
        yield return new WaitForSeconds(2f);
        hitBoxBody.Enable(damageJumpAOE);
        for (int i = 0; i < totalJump; i++) {
            Instantiate(vfx_impact, vfx_spawn_landing.position, Quaternion.identity);
            yield return new WaitForSeconds(3.167f);
        }
        hitBoxBody.Disable();
    }

    private IEnumerator DoAOEAttack() {
        sfx_AOE.Play();
        yield return new WaitForSeconds(3f);
        cameraShake.OnScreenShakeStart(durationAOEAttack - 4f);
        for(int i = 0; i < rockTargets.Length; i++) {
            sfx_jump[Random.Range(0, sfx_jump.Length)].Play();
            Instantiate(fallingRockProjectiles[Random.Range(0, fallingRockProjectiles.Length)],
                rockTargets[i].position, Quaternion.identity).GetComponent<Projectile>().Shoot(rockTargets[i].up, true, rockTargets[i].position - Vector3.up * rockFallingHeight);
            yield return new WaitForSeconds(0.25f);
        }
    }

    private IEnumerator WaitForPlayer() {
        ResetAll();
        yield return new WaitForSeconds(durationForPlayerToRecover);
        waitForPlayerRoutine = null;
        if (health < healthToStage3)
            StartCoroutine(BossBattleStage3());
        else if(health < healthToStage2)
            StartCoroutine(BossBattleStage2());
        else
            StartCoroutine(BossBattleStage1());
    }

    private void ResetAll() {
        Rock.enabled = false;
        currentProjectile = null;
        animator.SetBool(animParamRun, false);
        animator.SetBool(animParamWalk, false);
        animator.SetBool(animJumpAttack, false);
        animator.SetBool(animParamAOEAttack, false);
        hitBoxLeftHand.Disable();
        hitBoxRightHand.Disable();
        hitBoxBody.Disable();
        currentDestination = null;
        for (int i = 0; i < sfx_walk.Length; i++) {
            sfx_walk[i].Stop();
        }
        for (int i = 0; i < sfx_attackMeleeVariations.Length; i++) {
            sfx_attackMeleeVariations[i].Stop();
        }
        sfx_run.Stop();
        sfx_taunt1.Stop();
        sfx_taunt2.Stop();
        sfx_attackRanged.Stop();
        sfx_attackJump.Stop();
        sfx_AOE.Stop();
        for (int i = 0; i < sfx_jump.Length; i++) {
            if (sfx_jump[i].isPlaying) sfx_jump[i].Stop();
        }
        cameraShake.Stop();
    }

    private void Update() {
        if(health <= 0) {
            StopAllCoroutines();
            ResetAll();
            waitForPlayerRoutine = null;
            animator.SetTrigger(animParamDead);
            sfx_die.Play();
            health = healthMax;
            if (music_stage1.isPlaying) am.Fade(music_stage1, 0f, 1f);
            if (music_stage2.isPlaying) am.Fade(music_stage2, 0f, 1f);
            if (music_stage3.isPlaying) am.Fade(music_stage3, 0f, 1f);
            this.enabled = false;
            StartCoroutine(GameOver());
        }
        if(player.state == Enum.CharacterState.Dead) {
            StopAllCoroutines();
            ResetAll();
            vo_lines[Random.Range(0, vo_lines.Length)].Play();
            waitForPlayerRoutine = null;
            this.enabled = false;
        }
        if (player.state == Enum.CharacterState.Injured) {
            if(waitForPlayerRoutine == null) {
                StopAllCoroutines();
                vo_lines[Random.Range(0, vo_lines.Length)].Play();
                waitForPlayerRoutine = StartCoroutine(WaitForPlayer());
            }
        }
    }

    private IEnumerator GameOver() {
        yield return new WaitForSeconds(9f);
        gameOver_UI.Show();
        yield return new WaitForSeconds(6f);
        GameManager.Instance.GetTool<SceneController>("SceneController").LoadScene("MAIN", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    private void FixedUpdate() {
        if (currentDestination == null) return;
        Vector3 target = currentDestination.position;
        target.y = transform.position.y;
        if ((target - transform.position).magnitude <= stoppingDistance) {
            currentDestination = null;
            return;
        };
        transform.position += (target - transform.position).normalized * Time.fixedDeltaTime * currentSpeed;
        UpdateRotation(target);
    }

    private void UpdateRotation(Vector3 target, float lerp = 0) {
        target.y = transform.position.y;
        Vector3 eulerAngle = Quaternion.LookRotation((target - transform.position).normalized).eulerAngles;
        Quaternion rot_parent = Quaternion.identity;
        if (transform.parent) {
            Vector3 eulerAngle_parent = transform.parent.eulerAngles;
            eulerAngle_parent.x = eulerAngle_parent.z = 0f;
            rot_parent = Quaternion.Euler(eulerAngle_parent);
        }
        transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Inverse(rot_parent) * Quaternion.Euler(eulerAngle), (lerp == 0) ? Time.fixedDeltaTime * speedAngular : lerp);
    }

    private AudioSource sfx_takeDamage_current;

    public void TakeDamage(float damage) {
        health = Mathf.Clamp(health - damage, 0, healthMax);
        if (sfx_takeDamage_current == null || sfx_takeDamage_current.isPlaying == false) {
            sfx_takeDamage_current = sfx_takeDamage[Random.Range(0, sfx_takeDamage.Length)];
            sfx_takeDamage_current.Play();
        }
        UpdateHealth();
    }

    private void UpdateHealth() {
        UI_health.fillAmount = health / healthMax;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, meleeRange);
        if (player) {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(player.transform.position, transform.position);
        }
        if (rockTargets != null) {
            for (int i = 0; i < rockTargets.Length; i++)
                Gizmos.DrawSphere(rockTargets[i].position, 0.25f);
        }
    }

    protected float GetRelativeFloatValue(float a, float b, float ta, float tb, float tx) {
        return a + (b - a) * (tx - ta) / (tb - ta);
    }
}
