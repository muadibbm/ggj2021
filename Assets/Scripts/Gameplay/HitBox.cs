﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class HitBox : MonoBehaviour
{
    [SerializeField] GameObject [] blood;
    [SerializeField] GameObject bloodAttached;

    private float damage;
    private Collider col;

    private void Awake() {
        col = GetComponent<Collider>();
    }

    public void Enable(float damage) {
        this.damage = damage;
        col.enabled = true;
    }

    public void Disable() {
        col.enabled = false;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Boss") {
            if (blood != null && blood.Length != 0) {
                Vector3 direction = (other.transform.position - transform.position).normalized;
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + 180;
                var instance = Instantiate(blood[Random.Range(0, blood.Length)], other.transform.position, Quaternion.Euler(0, angle + 90, 0));
                instance.GetComponent<BFX_BloodSettings>().GroundHeight = 40.4f; // this is hardcoded
                //var boss = GameManager.Instance.GetBossController();
                //var nearestBone = GetNearestBone(boss.Hips, transform.position);
                //if (nearestBone != null) Instantiate(bloodAttached, nearestBone.position, nearestBone.rotation, nearestBone);
            }
            GameManager.Instance.GetBossController().TakeDamage(damage);
        }
        if (other.tag == "Player") {
            if (blood != null && blood.Length != 0) {
                Vector3 direction = (other.transform.position - transform.position).normalized;
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + 180;
                var instance = Instantiate(blood[Random.Range(0, blood.Length)], other.transform.position, Quaternion.Euler(0, angle + 90, 0));
                instance.GetComponent<BFX_BloodSettings>().GroundHeight = 40.4f; // this is hardcoded
                //var boss = GameManager.Instance.GetPlayerController();
                //var nearestBone = GetNearestBone(boss.Hips, transform.position);
                //if (nearestBone != null) Instantiate(bloodAttached, nearestBone.position, nearestBone.rotation, nearestBone);
            }
            GameManager.Instance.GetPlayerController().TakeDamage(damage);
        }
    }

    private Transform GetNearestBone(Transform characterTransform, Vector3 hitPos) {
        var closestPos = 10f;
        Transform closestBone = null;
        var childs = characterTransform.GetComponentsInChildren<Transform>();

        foreach (var child in childs) {
            var dist = Vector3.Distance(child.position, hitPos);
            if (dist < closestPos) {
                closestPos = dist;
                closestBone = child;
            }
        }

        var distRoot = Vector3.Distance(characterTransform.position, hitPos);
        if (distRoot < closestPos) {
            closestPos = distRoot;
            closestBone = characterTransform;
        }
        return closestBone;
    }
}