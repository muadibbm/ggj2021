﻿using UnityEngine;
using UnityEngine.Events;

public class NPCController : Interact
{
    [SerializeField] Dialogue current_dialouge;
    [SerializeField] Dialogue end_dialouge;
    [SerializeField] Camera cam;
    [SerializeField] Transform playerPivot;
    [SerializeField] string animIdleState = "Idle";
    [SerializeField] string animDialogueState = "Dialogue";
    [SerializeField] ObjectStateController startDialogueStateController;
    [SerializeField] bool disableOnDialougeEnd;

    private DialogueController dc;

    private Animator anim;
    private int animIdle;
    private int animDialogue;

    private void Awake() {
        dc = GameManager.Instance.GetTool<DialogueController>("DialogueController");
        anim = GetComponentInChildren<Animator>();
        if(anim) animIdle = Animator.StringToHash(animIdleState);
        if (anim) animDialogue = Animator.StringToHash(animDialogueState);
    }

    public override void BeginInteract(UnityAction action = null) {
        if(startDialogueStateController) startDialogueStateController.Activate();
        dc.ShowDialouge(current_dialouge, cam, playerPivot);
        if (anim) anim.Play(animDialogue);
    }

    public override void EndInteract() {
        // players cannot exit dialouge until finished it through DialogueController
        current_dialouge = end_dialouge;
        if (anim) anim.Play(animIdle);
        if (disableOnDialougeEnd) transform.parent.gameObject.SetActive(false);
    }

    public override void UpdateInput(float x, float z, Vector3 _forward, Vector3 _right) {
        dc.ProcessDialouge(z);
    }

    public void SetDialouge(Dialogue dialouge) {
        current_dialouge = dialouge;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(playerPivot.position, 0.1f);
        Gizmos.color = Color.white;
        Gizmos.DrawLine(playerPivot.position, transform.position);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(cam.transform.position, 0.1f);
        Gizmos.DrawLine(cam.transform.position, transform.position);
    }
}