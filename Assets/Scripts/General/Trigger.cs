﻿using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class Trigger : MonoBehaviour
{
    public Action<Collider> onEnter;
    public Action<Collider> onExit;

    private void OnTriggerEnter(Collider other) {
        if (onEnter != null) onEnter.Invoke(other);
    }

    private void OnTriggerExit(Collider other) {
        if (onExit != null) onExit.Invoke(other);
    }
}