﻿using UnityEngine;

public abstract class Interact : MonoBehaviour
{
    public string id;
    public abstract void UpdateInput(float x, float z, Vector3 _forward, Vector3 _right);
    public abstract void BeginInteract(UnityEngine.Events.UnityAction action = null);
    public abstract void EndInteract();
}
