﻿using UnityEngine;

public class ObjectStateEnabler : MonoBehaviour
{
    public GameObject[] gameObjects;
    public Component[] components;

    public void Enable() {
        ProcessObjects(true);
        ProcessComponents(true);
    }

    private void ProcessObjects(bool enable) {
        if (gameObjects != null) {
            for (int i = 0; i < gameObjects.Length; i++) {
                if (gameObjects[i].activeSelf) {
                    if (!enable) gameObjects[i].SetActive(false);
                } else {
                    if (enable) gameObjects[i].SetActive(true);
                }
            }
        }
    }

    private void ProcessComponents(bool enable) {
        if (components != null) {
            System.Type type;
            for (int i = 0; i < components.Length; i++) {
                type = components[i].GetType().BaseType;
                if (type == typeof(Animator)) {
                    var comp = components[i] as Animator;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                } else if (type == typeof(Collider)) {
                    var comp = components[i] as Collider;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                } else if (type == typeof(MonoBehaviour)) {
                    var comp = components[i] as MonoBehaviour;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                } else if (type == typeof(Renderer)) {
                    var comp = components[i] as Renderer;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                }
            }
        }
    }
}
