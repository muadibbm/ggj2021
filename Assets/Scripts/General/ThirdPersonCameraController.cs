﻿using UnityEngine;

public class ThirdPersonCameraController : MonoBehaviour
{
    [SerializeField] Vector3 closeOffset;
    [SerializeField] Vector3 farOffset;
    [SerializeField] float speed;
    [SerializeField] float speedAngular;
    [SerializeField] LayerMask mouseTargetLayer;
    [SerializeField] Transform mouseTarget;

    private Vector3 currentOffset;
    private Transform _transform;
    private Transform pTransform;
    private Transform cTranform;
    private Camera cam;
    private GameInput gi;
    private RaycastHit hitData;

    private void Awake() {
        cam = GetComponentInChildren<Camera>();
        cTranform = cam.transform;
        _transform = transform;
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        pTransform = GameManager.Instance.GetPlayer().transform;
    }

    private void Update()
    {
        UpdateZoom();
        UpdatePosition();
        UpdateRotation();
        UpdateMouseTarget();
    }

    private void UpdateMouseTarget() {
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hitData, 1000, mouseTargetLayer, QueryTriggerInteraction.Ignore)) {
            mouseTarget.position = hitData.point;
        }
    }

    public Vector3 GetMouseTarget() {
        return hitData.point;
    }

    public void SetFarOffset(Vector3 farOffset) {
        this.farOffset = farOffset;
    }

    public void SetCameraRotation(float x, float z) {
        Vector3 eulerAngles = cTranform.localRotation.eulerAngles;
        eulerAngles.x = x;
        eulerAngles.z = z;
        cTranform.localRotation = Quaternion.Euler(eulerAngles);
    }

    private void UpdateZoom() {
        currentOffset = Vector3.Lerp(currentOffset, farOffset, Time.deltaTime);
    }

    private void UpdatePosition() {
        _transform.position = Vector3.Lerp(_transform.position, pTransform.position, speed * Time.deltaTime);
        cTranform.localPosition = Vector3.Lerp(cTranform.localPosition, currentOffset, speed * Time.deltaTime);
    }

    private void UpdateRotation() {
        if(gi.HorizontalAxisR != 0f) _transform.Rotate(0f, gi.HorizontalAxisR * speedAngular * Time.deltaTime, 0f);
    }
}
