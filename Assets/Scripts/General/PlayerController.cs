﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    //public bool isIdle { get; private set; }
    public bool isInteracting { get; private set; }
    public bool isInDialouge { get; private set; }

    [SerializeField] ThirdPersonCameraController tpcc;
    [SerializeField] Vector3 combatCameraOffset;
    [SerializeField] Vector3 combatCameraRotation;
    [SerializeField] float speed;
    [SerializeField] float speedAngular;
    [SerializeField] float speedRoll;
    [SerializeField] float speedAngularAttack;
    [SerializeField] float durationRoll;
    [SerializeField] float duractionAttackMelee1;
    [SerializeField] float duractionAttackMelee2;
    [SerializeField] float duractionAttackMelee3;
    [SerializeField] float durectionToComboAttack;
    [SerializeField] float duractionAttackRanged;
    [SerializeField] float duractionKnockedBack;
    [SerializeField] float minWalkThreshold;
    [SerializeField] float gravityMultiplier;
    [SerializeField] float frontRaycastDist;
    [SerializeField] LayerMask frontRaycastMask;
    [SerializeField] MeshRenderer Arrow;
    [SerializeField] MeshRenderer Bow;
    [SerializeField] SkinnedMeshRenderer Mace;
    [SerializeField] GameObject arrowPrefab;
    [SerializeField] float healthMax;
    [SerializeField] UnityEngine.UI.Image UI_health;
    [SerializeField] UI_Panel UI_respawn;
    [SerializeField] Transform respawnPoint;
    [SerializeField] float timeToRespawn;

    [SerializeField] HitBox hitBox_Gorze;
    [SerializeField] float damageGorze_attack1;
    [SerializeField] float damageGorze_attack2;
    [SerializeField] float damageGorze_attack3;

    [SerializeField] AudioSource[] meleeAttack1Variations;
    [SerializeField] AudioSource[] meleeAttack2Variations;
    [SerializeField] AudioSource[] meleeAttack3Variations;
    [SerializeField] AudioSource[] rangedAttackVariations;
    [SerializeField] AudioSource[] rollVariations;
    [SerializeField] AudioSource[] sfx_takeDamage;

    public Transform Hips;

    public Enum.CharacterState state { get;  private set; }

    private Transform _transform;
    private Transform _parent;
    private Vector3 _forward;
    private Vector3 _right;

    private GameInput gi;
    private Transform camTransform;
    private SkinnedMeshRenderer rend;
    private Animator animator;
    private Interact interacting;
    private List<Interact> interactives;
    private CharacterController cc;
    private Projectile currentArrow;
    private AudioFootsteps audioFootsteps;

    private AudioSource attackMeleeAudio;
    private AudioSource attackRangedAudio;
    private AudioSource rollAudio;

    private Vector3 rollDir;
    private Vector3 prevPosition;
    private readonly string animWalk = "isRunning";
    private readonly string animSpeed = "Speed";
    private readonly string animMeleeAttack1 = "AttackMelee1";
    private readonly string animMeleeAttack2 = "AttackMelee2";
    private readonly string animMeleeAttack3 = "AttackMelee3";
    private readonly string animRoll = "Roll";
    private readonly string animRangedAttack = "AttackRanged";
    private readonly string animTakeDamage = "TakeDamage";
    private readonly string animDead = "isDead";
    private int animParamWalk;
    private int animParamSpeed;
    private int animParamMeleeAttack1;
    private int animParamMeleeAttack2;
    private int animParamMeleeAttack3;
    private int animParamRangedAttack;
    private int animParamRoll;
    private int animParamTakeDamage;
    private int animParamDead;
    private bool frontSideBlocked;
    private bool canComboAttack;
    private int comboAttackIndex;

    private Coroutine rollRoutine;
    private Coroutine meleeAttackRoutine;
    private Coroutine rangedAttackRoutine;

    private float health;

    private void Awake() {
        _transform = transform;
        rend = GetComponentInChildren<SkinnedMeshRenderer>();
        animator = GetComponentInChildren<Animator>();
        audioFootsteps = GetComponentInChildren<AudioFootsteps>();
        animParamWalk = Animator.StringToHash(animWalk);
        animParamSpeed = Animator.StringToHash(animSpeed);
        animParamMeleeAttack1 = Animator.StringToHash(animMeleeAttack1);
        animParamMeleeAttack2 = Animator.StringToHash(animMeleeAttack2);
        animParamMeleeAttack3 = Animator.StringToHash(animMeleeAttack3);
        animParamRangedAttack = Animator.StringToHash(animRangedAttack);
        animParamRoll = Animator.StringToHash(animRoll);
        animParamTakeDamage = Animator.StringToHash(animTakeDamage);
        animParamDead = Animator.StringToHash(animDead);
        prevPosition = _transform.position;
        interactives = new List<Interact>();
        cc = GetComponent<CharacterController>();
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        Trigger trigger = GetComponentInChildren<Trigger>();
        trigger.onEnter = RegisterInteractiveObject;
        trigger.onExit = ClearInteractiveObject;
        camTransform = Camera.main.transform;
        Arrow.enabled = Bow.enabled = false;
        health = healthMax;
        UpdateHealth();
    }

    private void OnDisable() {
        audioFootsteps.Stop();
    }

    public void StartCombat() {
        tpcc.SetCameraRotation(combatCameraRotation.x, combatCameraRotation.z);
        tpcc.SetFarOffset(combatCameraOffset);
        health = healthMax;
        UpdateHealth();
    }

    private void Update()
    {
        if (state == Enum.CharacterState.Injured || state == Enum.CharacterState.Dead) return;
        if (state == Enum.CharacterState.Walk) {
            audioFootsteps.Play();
        } else {
            audioFootsteps.Stop();
        }
        if (state == Enum.CharacterState.Roll) {
            DoRoll();
            return;
        }
        if (state == Enum.CharacterState.Attack_Range) {
            DoRangedAttack();
            return;
        }
        if (state == Enum.CharacterState.Attack_Melee) {
            DoAttackMelee();
            return;
        }
        UpdateDirection();
        UpdateInteraction();
        UpdateStates();
        UpdateAnimation();
        if (gi.AxisL.magnitude >= minWalkThreshold) {
            frontSideBlocked = Physics.Raycast(new Ray(_transform.position, _transform.forward), frontRaycastDist, frontRaycastMask, QueryTriggerInteraction.Ignore);
        }
        prevPosition = _transform.position;
    }

    public void EnterDialouge() {
        isInDialouge = true;
    }

    public void ExitDialouge() {
        isInDialouge = false;
        interacting.EndInteract();
        ClearInteractiveObject(interacting.GetComponent<Collider>());
        interacting = null;
        isInDialouge = false;
    }

    public void SetLocation(Transform location) {
        _parent = _transform.parent = location;
        _transform.localRotation = Quaternion.identity;
    }

    private void FixedUpdate() {
        if (state == Enum.CharacterState.Injured || state == Enum.CharacterState.Dead) return;
        if (state == Enum.CharacterState.Roll) {
            UpdateLocomotionToRoll();
            return;
        }
        if (_transform == null ||
            state == Enum.CharacterState.Attack_Melee ||
            state == Enum.CharacterState.Attack_Range) return;
        UpdateLocomotion();
        UpdateRotation();
    }

    private void DoRoll() {
        if (rollRoutine != null) return;
        Arrow.enabled = Bow.enabled = false; // in case it was in middle of ranged attack
        Mace.enabled = true;
        rollRoutine = StartCoroutine(_DoRoll());
    }

    private IEnumerator _DoRoll() {
        rollAudio = rollVariations[Random.Range(0, rollVariations.Length)];
        rollAudio.Play();
        animator.SetTrigger(animParamRoll);
        animator.SetBool(animParamWalk, false); // in case it was moving
        UpdateDirection();
        float time = 0f;
        rollDir = (_forward * gi.AxisL.z + _right * gi.AxisL.x).normalized;
        if (rollDir.magnitude == 0) rollDir = transform.forward.normalized;
        while (time < durationRoll) {
            UpdateRotationToRoll(rollDir);
            time += Time.deltaTime;
            yield return null;
        }
        state = Enum.CharacterState.Idle;
        rollRoutine = null;
        yield break;
    }

    private void DoAttackMelee() {
        if(canComboAttack) {
            if (gi.MouseButtonLeft) {
                canComboAttack = false;
                comboAttackIndex++;
                if (comboAttackIndex == 3) comboAttackIndex = 0; // reset combo
                if(meleeAttackRoutine != null) StopCoroutine(meleeAttackRoutine);
                meleeAttackRoutine = StartCoroutine(_DoAttackMelee());
            }
        } else {
            if (meleeAttackRoutine != null) return;
            meleeAttackRoutine = StartCoroutine(_DoAttackMelee());
        }
    }

    private IEnumerator _DoAttackMelee() {
        float duration;
        switch(comboAttackIndex) {
            case 1:
                animator.SetTrigger(animParamMeleeAttack2);
                duration = duractionAttackMelee2;
                attackMeleeAudio = meleeAttack2Variations[Random.Range(0, meleeAttack2Variations.Length)];
                hitBox_Gorze.Enable(damageGorze_attack2);
                break;
            case 2:
                animator.SetTrigger(animParamMeleeAttack3);
                duration = duractionAttackMelee3;
                attackMeleeAudio = meleeAttack3Variations[Random.Range(0, meleeAttack3Variations.Length)];
                hitBox_Gorze.Enable(damageGorze_attack3);
                break;
            default:
                animator.SetTrigger(animParamMeleeAttack1);
                duration = duractionAttackMelee1;
                attackMeleeAudio = meleeAttack1Variations[Random.Range(0, meleeAttack1Variations.Length)];
                hitBox_Gorze.Enable(damageGorze_attack1);
                break;
        }
        attackMeleeAudio.Play();
        animator.SetBool(animParamWalk, false); // in case it was moving
        UpdateDirection();
        Vector3 target = tpcc.GetMouseTarget();
        target.y = _transform.position.y;
        _forward = (target - _transform.position).normalized;
        _right = Quaternion.FromToRotation(Vector3.forward, Vector3.right) * _forward;
        float time = 0f;
        while (time < duration) {
            time += Time.deltaTime;
            UpdateRotationToAttack(time / duration);
            if (time >= (duration - durectionToComboAttack)) {
                canComboAttack = true;
            }
            if (gi.Roll) { // can do a roll in middle of melee attack
                state = Enum.CharacterState.Roll;
                meleeAttackRoutine = null;
                comboAttackIndex = 0;
                hitBox_Gorze.Disable();
                if (attackMeleeAudio != null) attackMeleeAudio.Stop();
                yield break;
            }
            yield return null;
        }
        hitBox_Gorze.Disable();
        transform.forward = _forward;
        transform.right = _right;
        canComboAttack = false;
        comboAttackIndex = 0;
        state = Enum.CharacterState.Idle;
        meleeAttackRoutine = null;
        yield break;
    }

    private void DoRangedAttack() {
        if (rangedAttackRoutine != null) return;
        rangedAttackRoutine = StartCoroutine(_DoRangedAttack());
    }

    private IEnumerator _DoRangedAttack() {
        currentArrow = null;
        Mace.enabled = false;
        attackRangedAudio = rangedAttackVariations[Random.Range(0, rangedAttackVariations.Length)];
        attackRangedAudio.Play();
        animator.SetTrigger(animParamRangedAttack);
        animator.SetBool(animParamWalk, false); // in case it was moving
        UpdateDirection();
        float time = 0f;
        Vector3 target;
        while (time < duractionAttackRanged) {
            Arrow.enabled = (time > 0.4f && time < 1.4f);
            Bow.enabled = (time > 0f && time < 2.25f);
            if(time > 1.4f) {
                if (currentArrow == null) {
                    currentArrow = Instantiate(arrowPrefab, Arrow.transform.position, Arrow.transform.rotation).GetComponent<Projectile>();
                    currentArrow.Shoot(_forward, false, Vector3.zero);
                }
            }
            target = tpcc.GetMouseTarget();
            target.y = _transform.position.y;
            _forward = (target - _transform.position).normalized;
            UpdateRotationToAttack(time / duractionAttackRanged);
            time += Time.deltaTime;
            if (gi.Roll) { // can do a roll in middle of ranged attack
                state = Enum.CharacterState.Roll;
                rangedAttackRoutine = null;
                if (attackRangedAudio != null) attackRangedAudio.Stop();
                yield break;
            }
            yield return null;
        }
        transform.forward = _forward;
        transform.right = Quaternion.FromToRotation(Vector3.forward, Vector3.right) * _forward;
        Mace.enabled = true;
        state = Enum.CharacterState.Idle;
        rangedAttackRoutine = null;
        yield break;
    }

    public void TakeDamage(float damage) {
        if (state == Enum.CharacterState.Injured) return;
        state = Enum.CharacterState.Injured;
        animator.SetBool(animParamWalk, false);
        StopAllCoroutines();
        rangedAttackRoutine = rollRoutine = meleeAttackRoutine = null;
        hitBox_Gorze.Disable();
        Arrow.enabled = Bow.enabled = false;
        Mace.enabled = true;
        audioFootsteps.Stop();
        if (attackRangedAudio != null) attackRangedAudio.Stop();
        if (attackMeleeAudio != null) attackMeleeAudio.Stop();
        if (rollAudio != null) rollAudio.Stop();
        health -= damage;
        UpdateHealth();
        sfx_takeDamage[Random.Range(0, sfx_takeDamage.Length)].Play();
        if (health <= 0f) {
            animator.SetBool(animParamDead, true);
            state = Enum.CharacterState.Dead;
            StartCoroutine(Respawn());
        } else {
            animator.SetTrigger(animParamTakeDamage);
            StartCoroutine(_StandUp());
        }
    }

    private IEnumerator Respawn() {
        UI_respawn.Show();
        yield return new WaitForSeconds(timeToRespawn);
        transform.position = respawnPoint.position;
        transform.rotation = respawnPoint.rotation;
        StartCombat();
        UI_respawn.Hide();
        GameManager.Instance.GetBossController().Respawn();
        animator.SetBool(animParamDead, false);
        yield return new WaitForSeconds(timeToRespawn - 1f);
        state = Enum.CharacterState.Idle;
        GameManager.Instance.GetBossController().StartCombat();
        GameManager.Instance.GetBossController().enabled = true;
    }

    private IEnumerator _StandUp() {
        yield return new WaitForSeconds(duractionKnockedBack);
        state = Enum.CharacterState.Idle;
    }

    private void UpdateHealth() {
        UI_health.fillAmount = health / healthMax;
    }

    private void OnDrawGizmos() {
        if (tpcc && _transform) {
            Vector3 target = tpcc.GetMouseTarget();
            Gizmos.DrawLine(transform.position, target);
            Gizmos.color = Color.red;
            target.y = _transform.position.y;
            Vector3 forw = (target - _transform.position).normalized;
            Gizmos.DrawRay(transform.position, forw);
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position, transform.forward);
        }
    }

    private void UpdateInteraction() {
        if (gi.InteractDown) OnInteract();
        if (gi.ReturnDown && interacting != null) OnReturn();
        if (interacting) interacting.UpdateInput(gi.HorizontalAxisL, gi.VerticalAxisL, _forward, _right);
        isInteracting = interacting != null;
        cc.enabled = !isInteracting;
    }

    private void OnInteract() {
        Interact _interacting = GetClosestInteracting();
        if (interacting == null && _interacting != null) {
            interacting = _interacting;
        }
        if (interacting) {
            interacting.BeginInteract();
        }
    }

    private void OnReturn() {
        if (isInDialouge) return;
        interacting.EndInteract();
        interacting = null;
        isInDialouge = false;
    }

    private void UpdateLocomotion() {
        if(state == Enum.CharacterState.Interacting) return;
        Vector3 motion = Vector3.zero;
        if (gi.AxisL.magnitude >= minWalkThreshold && frontSideBlocked == false) motion += (_forward * gi.AxisL.z + _right * gi.AxisL.x).normalized * speed;
        motion += gravityMultiplier * Constants.gravity * Time.fixedDeltaTime;
        cc.Move(motion * Time.fixedDeltaTime);
    }

    private void UpdateLocomotionToRoll() {
        Vector3 motion = Vector3.zero;
        motion += rollDir * speedRoll;
        motion += gravityMultiplier * Constants.gravity * Time.fixedDeltaTime;
        cc.Move(motion * Time.fixedDeltaTime);
    }

    public void UpdateDirection() {
        Vector3 camPos = camTransform.position;
        camPos.y = _transform.position.y;
        _forward = (_transform.position - camPos).normalized;
        _right = Quaternion.FromToRotation(Vector3.forward, Vector3.right) * _forward;
    }

    private void UpdateRotation() {
        if (state == Enum.CharacterState.Interacting) return;
        if (gi.AxisL.magnitude >= minWalkThreshold) {
            Vector3 eulerAngle = Quaternion.LookRotation(_forward * gi.AxisL.z + _right * gi.AxisL.x).eulerAngles;
            Quaternion rot_parent = Quaternion.identity;
            if(_parent) {
                Vector3 eulerAngle_parent = _parent.eulerAngles;
                eulerAngle_parent.x = eulerAngle_parent.z = 0f;
                rot_parent = Quaternion.Euler(eulerAngle_parent);
            }
            _transform.localRotation = Quaternion.Lerp(_transform.localRotation, Quaternion.Inverse(rot_parent) * Quaternion.Euler(eulerAngle), Time.deltaTime * speedAngular);
        }
    }

    private void UpdateRotationToAttack(float lerp) {
        Vector3 eulerAngle = (_forward != Vector3.zero) ? Quaternion.LookRotation(_forward).eulerAngles : Vector3.zero;
        Quaternion rot_parent = Quaternion.identity;
        if (_parent) {
            Vector3 eulerAngle_parent = _parent.eulerAngles;
            eulerAngle_parent.x = eulerAngle_parent.z = 0f;
            rot_parent = Quaternion.Euler(eulerAngle_parent);
        }
        _transform.localRotation = Quaternion.Lerp(_transform.localRotation, Quaternion.Inverse(rot_parent) * Quaternion.Euler(eulerAngle), lerp);
    }

    private void UpdateRotationToRoll(Vector3 dir) {
        Vector3 eulerAngle = Quaternion.LookRotation(dir).eulerAngles;
        Quaternion rot_parent = Quaternion.identity;
        if (_parent) {
            Vector3 eulerAngle_parent = _parent.eulerAngles;
            eulerAngle_parent.x = eulerAngle_parent.z = 0f;
            rot_parent = Quaternion.Euler(eulerAngle_parent);
        }
        _transform.localRotation = Quaternion.Lerp(_transform.localRotation, Quaternion.Inverse(rot_parent) * Quaternion.Euler(eulerAngle), Time.deltaTime * speedAngularAttack);
    }

    private void UpdateStates() {
        if(interacting) {
            state = Enum.CharacterState.Interacting;
            return;
        }
        if (gi.Roll) {
            state = Enum.CharacterState.Roll;
            return;
        }
        if (gi.MouseButtonRight) {
            state = Enum.CharacterState.Attack_Range;
            return;
        }
        if (gi.MouseButtonLeft) {
            state = Enum.CharacterState.Attack_Melee;
            return;
        }
        if (gi.MouseButtonRight) {
            state = Enum.CharacterState.Attack_Range;
            return;
        }
        if (gi.AxisL.magnitude >= minWalkThreshold) {
            state = Enum.CharacterState.Walk;
            return;
        }
        state = Enum.CharacterState.Idle;
    }

    private void UpdateAnimation() {
        switch(state) {
            case Enum.CharacterState.Idle:
                animator.SetBool(animParamWalk, false);
                animator.SetFloat(animParamSpeed, 1f);
                break;
            case Enum.CharacterState.Walk:
                animator.SetBool(animParamWalk, frontSideBlocked == false);
                animator.SetFloat(animParamSpeed, Mathf.Clamp01(gi.AxisL.magnitude + minWalkThreshold));
                break;
            case Enum.CharacterState.Interacting:
                animator.SetBool(animParamWalk, false);
                animator.SetFloat(animParamSpeed, 1f);
                break;
        }
    }

    private void RegisterInteractiveObject(Collider collider) {
        Interact interact = collider.GetComponent<Interact>();
        if (interact == null) return;
        if (interactives.Contains(interact) == false) interactives.Add(interact);
    }

    private void ClearInteractiveObject(Collider collider) {
        Interact interact = collider.GetComponent<Interact>();
        if (interact == null) return;
        if (interactives.Contains(interact)) interactives.Remove(interact);
    }

    private Interact GetClosestInteracting() {
        float min = Mathf.Infinity;
        Interact interact = null;
        float dist;
        for (int i = 0; i < interactives.Count; i++) {
            dist = Vector3.Distance(_transform.position, interactives[i].transform.position);
            if(dist < min) {
                interact = interactives[i];
                min = dist;
            }
        }
        return interact;
    }
}