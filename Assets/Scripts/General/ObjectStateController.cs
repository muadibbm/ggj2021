﻿using UnityEngine;
using System;

public class ObjectStateController : MonoBehaviour
{
    [SerializeField] Trigger trigger;
    [SerializeField] Action[] actions;

    private bool activated = false;

    [Serializable]
    public struct Action
    {
        public Enum.ObjectState state;
        public GameObject[] gameObjects;
        public Component[] components;
        public UI_Panel[] panels;
    }

    private void Awake() {
        if (trigger) trigger.onEnter = _OnTriggerEnter;
    }

    private void _OnTriggerEnter(Collider other) {
        Activate();
    }

    public void Activate() {
        if (activated) return;
        activated = true;
        for(int i = 0; i < actions.Length; i++) {
            ProcessComponents(actions[i].state == Enum.ObjectState.Enabled, actions[i]);
            ProcessPanels(actions[i].state == Enum.ObjectState.Enabled, actions[i]);
            ProcessObjects(actions[i].state == Enum.ObjectState.Enabled, actions[i]);
        }
    }

    private void ProcessPanels(bool enable, Action action) {
        if (action.panels != null) {
            for (int i = 0; i < action.panels.Length; i++) {
                if (action.panels[i].IsVisible) {
                    if (!enable) action.panels[i].Hide();
                } else {
                    if (enable) action.panels[i].Show();
                }
            }
        }
    }

    private void ProcessObjects(bool enable, Action action) {
        if (action.gameObjects != null) {
            for (int i = 0; i < action.gameObjects.Length; i++) {
                if (action.gameObjects[i].activeSelf) {
                    if (!enable) action.gameObjects[i].SetActive(false);
                } else {
                    if (enable) action.gameObjects[i].SetActive(true);
                }
            }
        }
    }

    private void ProcessComponents(bool enable, Action action) {
        if (action.components != null) {
            Type type;
            for (int i = 0; i < action.components.Length; i++) {
                type = action.components[i].GetType();
                if (type == typeof(Animator)) {
                    var comp = action.components[i] as Animator;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                } else if (type == typeof(Collider) || type.BaseType == typeof(Collider)) {
                    var comp = action.components[i] as Collider;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                } else if (type == typeof(MonoBehaviour) || type.BaseType == typeof(MonoBehaviour)) {
                    var comp = action.components[i] as MonoBehaviour;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                } else if (type == typeof(Renderer) || type.BaseType == typeof(Renderer)) {
                    var comp = action.components[i] as Renderer;
                    if (comp.enabled) {
                        if (!enable) comp.enabled = false;
                    } else {
                        if (enable) comp.enabled = true;
                    }
                }
            }
        }
    }
}
