﻿using UnityEngine;

public class UI_ESC : MonoBehaviour
{
    [SerializeField] GameObject escMenu;
    [SerializeField] UnityEngine.UI.Image tutorial;
    private GameInput gi;
    private bool isVisible;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        isVisible = false;
    }

    private void Update() {
        if(gi.Quit) {
            if (GameManager.Instance.GetPlayerController().enabled == false) return;
            isVisible = !isVisible;
            escMenu.SetActive(isVisible);
            tutorial.enabled = isVisible;
            Cursor.visible = isVisible;
            Time.timeScale = (isVisible) ? 0 : 1;
        }
    }

    public void ApplicationQuit() {
        Application.Quit();
    }
}
