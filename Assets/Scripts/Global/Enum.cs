﻿public class Enum
{
    public enum BoatState { Idle, Moving, Docking, Docked }
    public enum CharacterState { Idle, Walk, Interacting, Swim, Swim_Idle, Attack_Melee, Attack_Range, Defend, Roll, Hit_React, Dead, Attack_AOE, Attack_Jump, Run, Injured }
    public enum FishBehaviour { Idle, Follow }
    public enum ObjectState { Enabled, Disabled }
    public enum GroundType { Dirt, Stone, Grass }
    public enum ActivationType {OnEnable, OnTriggerEnter, OnTriggerExit };
}