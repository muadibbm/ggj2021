﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
	private readonly Dictionary<string, Component> tools = new Dictionary<string, Component>();

	public static GameManager Instance {
		get { return GetInstance(); }
		set { _instance = value; }
	}

	private static GameManager _instance;
	private static GameObject _player;
	private static GameObject _boss;
	private static PlayerController _playerController;
	private static BossController _bossController;

	private static GameManager GetInstance() {
		if (ReferenceEquals(_instance, null)) {
			var go = new GameObject("GameManager");
			DontDestroyOnLoad(go);
			_instance = go.AddComponent<GameManager>();
		}
		return _instance;
	}

	private void Awake() {
		this.AddTool<GameInput>("GameInput");
		this.AddTool<AudioManager>("AudioManager");
		this.AddTool<SceneController>("SceneController");
		Cursor.visible = false;
	}

	public GameObject GetPlayer() {
		if (_player == null) _player = GameObject.FindGameObjectWithTag("Player");
		return _player;
	}

	public GameObject GetBoss() {
		if (_boss == null) _boss = GameObject.FindGameObjectWithTag("Boss");
		return _boss;
	}

	public PlayerController GetPlayerController() {
		if (_playerController == null && GetPlayer()) _playerController = GetPlayer().GetComponent<PlayerController>();
		return _playerController;
	}

	public BossController GetBossController() {
		if (_bossController == null) _bossController = GetBoss().GetComponent<BossController>();
		return _bossController;
	}

	public ObjType GetTool<ObjType>(string objName) where ObjType : Component {
		return tools[objName] as ObjType;
	}

	public ObjType AddExistingTool<ObjType>() where ObjType : Component {
		ObjType obj = FindObjectOfType<ObjType>();
		if (obj == null) return null;
		obj.transform.SetParent(this.transform);
		this.tools.Add(obj.name, obj);
		return obj;
	}

	public ObjType AddTool<ObjType>(string objName) where ObjType : Component {
		var tool = new GameObject(objName);
		tool.transform.SetParent(this.transform);
		ObjType obj = tool.AddComponent<ObjType>();
		this.tools.Add(objName, obj);
		return obj;
	}
}
