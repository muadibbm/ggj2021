﻿using UnityEngine;

public class WaveController : MonoBehaviour
{
    public Vector3 waveSource = new Vector3(3000f, 0f, 3000f);
    public float wave1Speed = 0.1f;
    public float wave1Length = 50f;
    public float wave1Amplitude = 1f;

    public Vector3 ApplyWave(Vector3 point) {
        point.y = 0.0f;
        float dist = Vector3.Distance(point, waveSource);
        dist = (dist % wave1Length) / wave1Length;
        float y1 = wave1Amplitude * Mathf.Sin(2f * Mathf.PI * (Time.time * wave1Speed + dist));
        //dist = (dist % wc.wave2Length) / wc.wave2Length;
        //float y2 = wc.wave2Amplitude * Mathf.Sin(Mathf.PI * (Time.time * wc.wave2Speed + dist));
        point.y = y1; // + y2
        return point;
    }
}
