﻿using UnityEngine;

public class GameInput : MonoBehaviour
{
    public bool AnyKeyDown { get; private set; }
    public bool MouseButtonLeft { get; private set; }
    public bool MouseButtonRight { get; private set; }
    public bool MouseButtonLeftDown { get; private set; }
    public bool MouseButtonRightDown { get; private set; }
    public Vector2 MousePosition { get; private set; }
    public Vector2 MouseScrollDelta { get; private set; }
    public float HorizontalAxisL { get; private set; }
    public float VerticalAxisL { get; private set; }
    public float HorizontalAxisR { get; private set; }
    //public float VerticalAxisR { get; private set; }
    public Vector3 AxisL { get; private set; }
    //public Vector3 AxisR { get; private set; }
    public bool InteractDown { get; private set; }
    public bool ReturnDown { get; private set; }
    public bool Roll { get; private set; }
    public bool Quit { get; private set; }

    public void SetCursorVisibility(bool val) {
        Cursor.visible = val;
    }

    private void Update() {
        AnyKeyDown = Input.anyKeyDown;
        MouseButtonLeft = Input.GetMouseButton(0);
        MouseButtonRight = Input.GetMouseButton(1);
        MouseButtonLeftDown = Input.GetMouseButtonDown(0);
        MouseButtonRightDown = Input.GetMouseButtonDown(1);
        MousePosition = Input.mousePosition;
        MouseScrollDelta = Input.mouseScrollDelta;
        HorizontalAxisL = Input.GetAxis("HorizontalL");
        VerticalAxisL = Input.GetAxis("Vertical");
        AxisL = new Vector3(HorizontalAxisL, 0f, VerticalAxisL);
        HorizontalAxisR = Input.GetAxis("HorizontalR");
        //VerticalAxisR = Input.GetAxis("Mouse Y");
        //AxisR = new Vector3(HorizontalAxisR, 0f, VerticalAxisR);
        InteractDown = Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");
        ReturnDown = Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Cancel");
        Roll = Input.GetKeyDown(KeyCode.Space);
        Quit = Input.GetKeyDown(KeyCode.Escape);
    }
}