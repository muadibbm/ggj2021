﻿using UnityEngine;

public class WaveBasedObject : MonoBehaviour
{
    [SerializeField] float rotationIntensity = 1f;
    [SerializeField] float rotationSensitivity = 1f;
    [SerializeField] Transform frontR;
    [SerializeField] Transform frontL;
    [SerializeField] Transform backR;
    [SerializeField] Transform backL;

    private WaveController wc;

    private void Awake() {
        wc = GameManager.Instance.GetTool<WaveController>("WaveController");
    }

    private void Update()
    {
        transform.position = wc.ApplyWave(transform.position);

        if (frontR == null || frontL == null || backR == null || backL == null) return;

        frontR.localPosition = transform.rotation * (new Vector3(1, 0, 1) * rotationSensitivity);
        frontL.localPosition = transform.rotation * (new Vector3(-1, 0, 1) * rotationSensitivity);
        backR.localPosition = transform.rotation * (new Vector3(1, 0, -1) * rotationSensitivity);
        backL.localPosition = transform.rotation * (new Vector3(-1, 0, -1) * rotationSensitivity);

        float yFrontR = wc.ApplyWave(transform.position + frontR.localPosition / wc.wave1Amplitude).y;
        float yFrontL = wc.ApplyWave(transform.position + frontL.localPosition / wc.wave1Amplitude).y;
        float yBackR = wc.ApplyWave(transform.position + backR.localPosition / wc.wave1Amplitude).y;
        float yBackL = wc.ApplyWave(transform.position + backL.localPosition / wc.wave1Amplitude).y;

        // TODO : fix the issue with negative multiplication when rotating the object is externally being rotated
        Vector3 rot = transform.rotation.eulerAngles;
        rot.z = ((yFrontR + yBackR) / 2f - (yFrontL + yBackL) / 2f) * rotationIntensity;
        rot.x = ((yFrontR + yFrontL) / 2f - (yBackR + yBackL) / 2f) * -rotationIntensity;
        transform.rotation = Quaternion.Euler(rot);
    }

    private void OnDrawGizmos() {
        if (wc == null) return;
        if (frontR == null || frontL == null || backR == null || backL == null) return;
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + frontR.localPosition * wc.wave1Amplitude, 0.1f);
        Gizmos.DrawSphere(transform.position + frontL.localPosition * wc.wave1Amplitude, 0.1f);
        Gizmos.DrawSphere(transform.position + backR.localPosition * wc.wave1Amplitude, 0.1f);
        Gizmos.DrawSphere(transform.position + backL.localPosition * wc.wave1Amplitude, 0.1f);
    }
}
 