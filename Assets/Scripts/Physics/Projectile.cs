﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    [SerializeField] float speedForward;
    [SerializeField] float speedUpward;
    [SerializeField] float lifeTime;
    [SerializeField] float damage;
    [SerializeField] Transform mesh;
    [SerializeField] bool destroyOnImpact;
    [SerializeField] float destoryDelay = 1f;
    [SerializeField] GameObject vfx;
    [SerializeField] GameObject sfx;
    [SerializeField] GameObject[] blood;
    [SerializeField] GameObject bloodAttached;
    [SerializeField] float stoppingDistance = 0.1f;

    private bool shot;
    private float time;
    private Vector3 prevPos;
    private Vector3 target;
    private bool hasTarget;
    private float random;
    private bool hasHit;

    public void Shoot(Vector3 forward, bool hasTarget, Vector3 target) {
        shot = true;
        transform.forward = forward;
        prevPos = transform.position;
        this.target = target;
        this.hasTarget = hasTarget;
        random = Random.Range(0, 1f);
    }

    private void FixedUpdate() {
        if (shot) {
            if (hasTarget) {
                transform.position += (target - transform.position).normalized * speedForward * Time.fixedDeltaTime;
            } else {
                transform.position += (transform.forward * speedForward + random * transform.up * speedUpward) * Time.fixedDeltaTime;
            }
            mesh.forward = (transform.position - prevPos).normalized;
            prevPos = transform.position;
        }
    }

    private void Update() {
        time += Time.deltaTime;
        if (time > lifeTime || (transform.position - target).magnitude <= stoppingDistance) {
            DoEffect();
            StartCoroutine(Destory());
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (hasHit) return;
        hasHit = true;
        if (other.tag == "Boss") {
            transform.parent = other.transform;
            GameManager.Instance.GetBossController().TakeDamage(damage);
            if (blood != null && blood.Length != 0) {
                Vector3 direction = (other.transform.position - transform.position).normalized;
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + 180;
                var instance = Instantiate(blood[Random.Range(0, blood.Length)], other.transform.position, Quaternion.Euler(0, angle + 90, 0));
                instance.GetComponent<BFX_BloodSettings>().GroundHeight = 40.4f; // this is hardcoded
                //var boss = GameManager.Instance.GetBossController();
                //var nearestBone = GetNearestBone(boss.Hips, transform.position);
                //if (nearestBone != null) Instantiate(bloodAttached, nearestBone.position, nearestBone.rotation, nearestBone);
            }
        }
        if (other.tag == "Player") {
            GameManager.Instance.GetPlayerController().TakeDamage(damage);
            if (blood != null && blood.Length != 0) {
                Vector3 direction = (other.transform.position - transform.position).normalized;
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + 180;
                var instance = Instantiate(blood[Random.Range(0, blood.Length)], other.transform.position, Quaternion.Euler(0, angle + 90, 0));
                instance.GetComponent<BFX_BloodSettings>().GroundHeight = 40.4f; // this is hardcoded
                //var boss = GameManager.Instance.GetPlayerController();
                //var nearestBone = GetNearestBone(boss.Hips, transform.position);
                //if (nearestBone != null) Instantiate(bloodAttached, nearestBone.position, nearestBone.rotation, nearestBone);
            }
        }
        DoEffect();
        if (destroyOnImpact) StartCoroutine(Destory());
        else enabled = false;
    }

    private IEnumerator Destory() {
        yield return new WaitForSeconds(destoryDelay);
        Destroy(gameObject);
    }

    private void DoEffect() {
        if (vfx) Instantiate(vfx, transform.position, Quaternion.identity);
        if (sfx) Instantiate(sfx, transform.position, Quaternion.identity);
    }

    private Transform GetNearestBone(Transform characterTransform, Vector3 hitPos) {
        var closestPos = 10f;
        Transform closestBone = null;
        var childs = characterTransform.GetComponentsInChildren<Transform>();

        foreach (var child in childs) {
            var dist = Vector3.Distance(child.position, hitPos);
            if (dist < closestPos) {
                closestPos = dist;
                closestBone = child;
            }
        }

        var distRoot = Vector3.Distance(characterTransform.position, hitPos);
        if (distRoot < closestPos) {
            closestPos = distRoot;
            closestBone = characterTransform;
        }
        return closestBone;
    }
}